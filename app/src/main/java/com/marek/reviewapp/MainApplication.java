package com.marek.reviewapp;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.marek.reviewapp.ratethings.android.realm.RealmDB;
import com.marek.reviewapp.di.DaggerMainApplicationComponent;
import com.marek.reviewapp.di.MainApplicationComponent;
import com.marek.reviewapp.di.MainApplicationModule;
import com.marek.reviewapp.ratethings.di.DaggerRateThingsComponent;
import com.marek.reviewapp.ratethings.di.RateThingsComponent;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.io.File;

/**
 * Created by marek on 16.07.16.
 */
public class MainApplication extends Application {

    private static Context APP_CONTEXT;
    private static Picasso PICASSO_INSTANCE;
    private static MainApplicationComponent mAppComponent;
    private static RealmDB mRealm;

    @Override
    public void onCreate() {
        super.onCreate();

        APP_CONTEXT = getApplicationContext();

        mAppComponent = DaggerMainApplicationComponent.builder()
                .mainApplicationModule(new MainApplicationModule()).build();

        initRealm();

        if (isDebug()) {
            initStetho();
        }
    }

    private void initRealm() {
        mRealm = new RealmDB();
        mRealm.init(APP_CONTEXT);
    }

    private void initStetho() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());
    }

    public static Picasso getPicasso() {
        if (PICASSO_INSTANCE == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setCache(createHttpClientCache(MainApplication.getAppContext()));
            Picasso.Builder builder = new Picasso.Builder(MainApplication.getAppContext()).downloader(new OkHttpDownloader(okHttpClient));
            PICASSO_INSTANCE = builder.loggingEnabled(isDebug()).build();
            PICASSO_INSTANCE.setIndicatorsEnabled(isDebug());
        }
        return PICASSO_INSTANCE;
    }

    public static Cache createHttpClientCache(Context context) {
        int cacheSize = 20 * 1024 * 1024; // 20MiB
        File cacheDir = context.getDir("app_http_cache", Context.MODE_PRIVATE);
        return new Cache(cacheDir, cacheSize);
    }


    public static Context getAppContext() {
        return APP_CONTEXT;
    }

    public static MainApplicationComponent getAppComponent() {
        return mAppComponent;
    }

    public static boolean isDebug() {
        return getAppContext().getResources().getBoolean(R.bool.is_debug);
    }
}

