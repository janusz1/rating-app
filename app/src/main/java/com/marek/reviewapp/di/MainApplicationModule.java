package com.marek.reviewapp.di;

import com.google.gson.Gson;
import com.marek.reviewapp.ratethings.android.repository.LocalDBRatingRepository;
import com.marek.reviewapp.ratethings.domain.repository.RatingRepository;
import com.marek.reviewapp.ratethings.presenter.PagedThingPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by marek on 16.07.16.
 */
@Module
public class MainApplicationModule {

    @Provides @Singleton
    public Gson provideGson() {
        return new Gson();
    }

}
