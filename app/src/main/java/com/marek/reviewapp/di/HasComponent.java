package com.marek.reviewapp.di;

/**
 * Created by marek on 18.07.16.
 */
public interface HasComponent<C> {
    C getComponent();
}
