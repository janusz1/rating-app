package com.marek.reviewapp.di;

import com.marek.reviewapp.MainActivity;
import com.marek.reviewapp.ratethings.android.repository.LocalJsonThingRepository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by marek on 16.07.16.
 */
@Singleton
@Component(modules = { MainApplicationModule.class })
public interface MainApplicationComponent {
    void inject(LocalJsonThingRepository localJsonThingRepository);
    void inject(MainActivity mainActivity);
}
