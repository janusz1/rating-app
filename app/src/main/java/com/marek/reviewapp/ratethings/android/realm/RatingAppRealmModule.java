package com.marek.reviewapp.ratethings.android.realm;

import com.marek.reviewapp.ratethings.domain.model.RatingEntityRealm;

import io.realm.annotations.RealmModule;

/**
 * Created by marek on 16.07.16.
 */
@RealmModule(classes = {RatingEntityRealm.class})
public class RatingAppRealmModule {
}
