package com.marek.reviewapp.ratethings.di;

import com.marek.reviewapp.ratethings.android.repository.LocalDBRatingRepository;
import com.marek.reviewapp.ratethings.android.repository.LocalJsonThingRepository;
import com.marek.reviewapp.ratethings.domain.model.RatingEntityList;
import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;
import com.marek.reviewapp.ratethings.domain.repository.FetchThingsRepository;
import com.marek.reviewapp.ratethings.domain.usecase.FetchThingsUseCase;
import com.marek.reviewapp.ratethings.domain.usecase.RateThingUseCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by marek on 18.07.16.
 */
@Module
public class RateThingsModule {

    @Provides
    FetchThingsUseCase provideFetchThingsUseCase(FetchThingsRepository<ThingEntityList> thingRepo, FetchThingsRepository<RatingEntityList> ratingRepo) {
        return new FetchThingsUseCase(thingRepo, ratingRepo);
    }

    @Provides
    @Singleton
    FetchThingsRepository<ThingEntityList> provideThingEntityRepository() {
        return new LocalJsonThingRepository();
    }

    @Provides
    @Singleton
    FetchThingsRepository<RatingEntityList> provideRatingEntityRepository() {
        return new LocalDBRatingRepository();
    }

    @Provides
    RateThingUseCase provideRateThingUseCase() {
        return new RateThingUseCase();
    }
}
