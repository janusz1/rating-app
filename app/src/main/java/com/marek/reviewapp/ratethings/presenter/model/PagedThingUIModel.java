package com.marek.reviewapp.ratethings.presenter.model;

import java.util.List;

/**
 * Created by marek on 16.07.16.
 */
public class PagedThingUIModel {

    private List<List<ThingUIModel>> mPagedUIModels;

    public List<ThingUIModel> getThingUIModelPage(int pageNum) {
        return mPagedUIModels.get(pageNum);
    }

    public int getNumPages() {
        return mPagedUIModels.size();
    }

    public void setThingUIModelPageList(List<List<ThingUIModel>> pagedUIModels) {
        mPagedUIModels = pagedUIModels;
    }
}
