package com.marek.reviewapp.ratethings.domain.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by marek on 16.07.16.
 */
public class RatingEntityRealm extends RealmObject implements RatingEntity {

    public static final String FIELD_NAME_THING_ID = "thingId";

    @PrimaryKey
    private int thingId;
    private int rating;

    public int getThingId() {
        return thingId;
    }

    public void setThingId(int thingId) {
        this.thingId = thingId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
