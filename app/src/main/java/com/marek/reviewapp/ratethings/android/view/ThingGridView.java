package com.marek.reviewapp.ratethings.android.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.marek.reviewapp.R;
import com.marek.reviewapp.ratethings.android.adapter.ThingAdapter;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;

/**
 * Created by marek on 17.07.16.
 */
public class ThingGridView extends RecyclerView {

    public ThingGridView(Context context) {
        super(context);
    }

    public ThingGridView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ThingGridView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init() {
        GridLayoutManager layoutManager = new NoScrollGridLayoutManager(getContext(), getContext().getResources().getInteger(R.integer.thing_grid_size));
        setLayoutManager(layoutManager);
    }

    public void updateThingRating(ThingUIModel thing) {
        ThingAdapter thingGridAdapter = (ThingAdapter) getAdapter();
        ImageView itemRatedOverlay = (ImageView) getLayoutManager().findViewByPosition(thingGridAdapter.getThingPosition(thing))
                .findViewById(R.id.grid_item_rated_overlay);
        thingGridAdapter.updateThingRatingOverlay(itemRatedOverlay, thing);
    }

}
