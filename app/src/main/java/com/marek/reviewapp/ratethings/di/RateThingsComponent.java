package com.marek.reviewapp.ratethings.di;

import com.marek.reviewapp.ratethings.android.PagedThingsFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by marek on 18.07.16.
 */
@Singleton
@Component(modules = {RateThingsModule.class})
public interface RateThingsComponent {
    void inject(PagedThingsFragment pagedThingsFragment);
}
