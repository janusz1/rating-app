package com.marek.reviewapp.ratethings.domain.repository;

import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;

import rx.Observable;

/**
 * Created by marek on 16.07.16.
 */
public interface ThingRepository extends FetchThingsRepository<ThingEntityList>{

    @Override
    Observable<ThingEntityList> fetchThingData();

}
