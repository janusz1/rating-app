package com.marek.reviewapp.ratethings.view;

import com.marek.reviewapp.ratethings.presenter.model.PagedThingUIModel;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;

/**
 * Created by marek on 16.07.16.
 */
public interface PageView {

    void showPagedThingGrids(PagedThingUIModel pagedThingUIModel);
    void markItemRated(ThingUIModel thingUIModel);
    void showFetchThingsError();
}
