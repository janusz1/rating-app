package com.marek.reviewapp.ratethings.android.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marek.reviewapp.MainApplication;
import com.marek.reviewapp.R;
import com.marek.reviewapp.ratethings.android.view.ReviewOverlayView;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;

import java.util.List;

/**
 * Created by marek on 17.07.16.
 */
public class ThingAdapter extends RecyclerView.Adapter<ThingVH> {

    private final int GRID_ITEM_SIZE_PX;

    private List<ThingUIModel> mThingUIModels;
    private Context mContext;
    private OnThingClickListener mOnThingClickListener;

    public interface OnThingClickListener {
        void onThingClick(ThingUIModel thingUIModel);
    }

    public ThingAdapter(Context context, List<ThingUIModel> gridEntities, OnThingClickListener onThingClickListener) {
        this.mThingUIModels = gridEntities;
        mContext = context;
        mOnThingClickListener = onThingClickListener;
        GRID_ITEM_SIZE_PX = mContext.getResources().getDimensionPixelSize(R.dimen.grid_item_size);
    }

    public void updateThingRatingOverlay(ImageView ratedOverlay, ThingUIModel thingUIModel) {
        if (thingUIModel.getRating() == null)
            return;
        ReviewOverlayView.RatingUI rating = ReviewOverlayView.RatingUI.fromInteger(thingUIModel.getRating());
        Drawable ratingDrawable = rating.getDrawable();
        ratedOverlay.setImageDrawable(ratingDrawable);
        ratedOverlay.setVisibility(View.VISIBLE);
    }

    @Override
    public ThingVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.grid_item_layout, null);
        return new ThingVH(view);
    }

    @Override
    public void onBindViewHolder(ThingVH viewHolder, int position) {
        final ThingUIModel thing = mThingUIModels.get(position);

        String thumbUrl = thing.getThumbnail();

        MainApplication.getPicasso().load(thumbUrl)
                .resize(GRID_ITEM_SIZE_PX, GRID_ITEM_SIZE_PX)
                .into(viewHolder.thingImage);
        viewHolder.thingLabel.setText(thing.getLabel());
        updateThingRatingOverlay(viewHolder.ratedOverlay, thing);

        viewHolder.bindThingClickListener(thing, mOnThingClickListener);

    }

    @Override
    public int getItemCount() {
        return mThingUIModels.size();
    }

    public int getThingPosition(ThingUIModel thing) {
        return mThingUIModels.indexOf(thing);
    }

}

class ThingVH extends RecyclerView.ViewHolder {
    ImageView thingImage;
    TextView thingLabel;
    ImageView ratedOverlay;

    ThingVH(View layout) {
        super(layout);
        thingImage = (ImageView) layout.findViewById(R.id.grid_item_image);
        thingLabel = (TextView) layout.findViewById(R.id.grid_item_label);
        ratedOverlay = (ImageView) layout.findViewById(R.id.grid_item_rated_overlay);
    }

    public void bindThingClickListener(final ThingUIModel thing, final ThingAdapter.OnThingClickListener onThingClickListener) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onThingClickListener.onThingClick(thing);
            }
        });
    }
}


