package com.marek.reviewapp.ratethings.domain.repository;

import rx.Observable;

/**
 * Created by marek on 16.07.16.
 */
public interface FetchThingsRepository<T> {

    Observable<T> fetchThingData();
}
