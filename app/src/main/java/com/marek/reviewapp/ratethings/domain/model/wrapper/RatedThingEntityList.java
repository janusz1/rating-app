package com.marek.reviewapp.ratethings.domain.model.wrapper;

import com.marek.reviewapp.ratethings.domain.model.RatingEntity;
import com.marek.reviewapp.ratethings.domain.model.RatingEntityList;
import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by marek on 16.07.16.
 */
public class RatedThingEntityList {

    private final ThingEntityList mThingEntityList;
    private final Map<Integer, Integer> mThingIdRatingMap;

    public RatedThingEntityList(ThingEntityList thingEntityList, RatingEntityList ratingEntityList) {
        mThingEntityList = thingEntityList;
        mThingIdRatingMap = getThingIdRatingMap(ratingEntityList);
    }

    private Map<Integer, Integer> getThingIdRatingMap(RatingEntityList ratingEntityList) {
        Map<Integer, Integer> map = new HashMap<>();
        List<? extends RatingEntity> ratingEntities = ratingEntityList.getRatingEntities();
        if (ratingEntities != null) {
            for (RatingEntity ratingEntity : ratingEntities) {
                map.put(ratingEntity.getThingId(), ratingEntity.getRating());
            }
        }
        return map;
    }

    public ThingEntityList getThingEntityList() {
        return mThingEntityList;
    }

    public Map<Integer, Integer> getThingIdRatingMap() {
        return mThingIdRatingMap;
    }
}
