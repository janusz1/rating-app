package com.marek.reviewapp.ratethings.presenter.transformer;

import android.support.annotation.NonNull;

import com.marek.reviewapp.ratethings.domain.model.ThingEntity;
import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;
import com.marek.reviewapp.ratethings.presenter.model.PagedThingUIModel;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by marek on 16.07.16.
 */
public class ThingTransformer {

    public PagedThingUIModel getPagedThingUIModel(@NonNull ThingEntityList thingEntityList,
                                                  @NonNull Map<Integer, Integer> thingRatings,
                                                  int pageSize) {
        List<ThingEntity> thingEntities = thingEntityList.getThingEntities();
        List<List<ThingUIModel>> thingUIModelPageList = new ArrayList<>();

        List<ThingUIModel> thingUIModels = null;
        for (int entityCount = 0; entityCount < thingEntities.size(); ++entityCount) {
            if (entityCount % pageSize == 0) {
                thingUIModels = new ArrayList<>();
                thingUIModelPageList.add(thingUIModels);
            }
            thingUIModels.add(getThingUIModel(thingEntities.get(entityCount), thingRatings));
        }

        PagedThingUIModel pagedUIModel = new PagedThingUIModel();
        pagedUIModel.setThingUIModelPageList(thingUIModelPageList);

        return pagedUIModel;
    }

    private ThingUIModel getThingUIModel(@NonNull ThingEntity thingEntity, @NonNull Map<Integer, Integer> ratings) {
        ThingUIModel uiModel = new ThingUIModel();
        uiModel.setLabel(thingEntity.getLabel());
        uiModel.setThumbnail(thingEntity.getThumbnail());

        Integer thingId = thingEntity.getThingId();
        uiModel.setThingId(thingId);
        uiModel.setRating(ratings.get(thingId));
        return uiModel;
    }

}
