package com.marek.reviewapp.ratethings.domain.usecase;

import com.marek.reviewapp.ratethings.android.repository.LocalDBRatingRepository;
import com.marek.reviewapp.ratethings.domain.repository.RatingRepository;

/**
 * Created by marek on 16.07.16.
 */
public class RateThingUseCase {

    // inject
    RatingRepository mRatingRepository = new LocalDBRatingRepository();

    public void rateThing(Integer thingId, Integer rating) {
        mRatingRepository.createOrUpdate(thingId, rating);      // small op TODO async
    }
}
