package com.marek.reviewapp.ratethings.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marek.reviewapp.R;
import com.marek.reviewapp.android.BaseDependencyFragment;
import com.marek.reviewapp.android.CanHandleBackPress;
import com.marek.reviewapp.ratethings.android.adapter.ThingAdapter;
import com.marek.reviewapp.ratethings.android.adapter.ThingGridPagerAdapter;
import com.marek.reviewapp.ratethings.android.view.ReviewOverlayView;
import com.marek.reviewapp.ratethings.android.view.ThingGridView;
import com.marek.reviewapp.ratethings.android.view.ThingGridViewPager;
import com.marek.reviewapp.ratethings.di.RateThingsComponent;
import com.marek.reviewapp.ratethings.presenter.PagedThingPresenter;
import com.marek.reviewapp.ratethings.presenter.model.PagedThingUIModel;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;
import com.marek.reviewapp.ratethings.view.PageView;

import javax.inject.Inject;

/**
 * Created by marek on 18.07.16.
 */
public class PagedThingsFragment extends BaseDependencyFragment implements PageView, CanHandleBackPress {

    public static final String BACKSTACK_TAG = "com.marek.reviewapp.ratethings.android.PagedThingsFragment";

    @Inject
    PagedThingPresenter mPagePresenter;

    private RatingOverlayDelegate mRatingOverlayDelegate;
    private ThingGridViewPager mViewPager;

    public static PagedThingsFragment newInstance() {
        PagedThingsFragment fragment = new PagedThingsFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paged_things_layout, container, false);
        mViewPager = (ThingGridViewPager) view.findViewById(R.id.fragment_paged_things_viewpager);
        mRatingOverlayDelegate = new RatingOverlayDelegate(mOnThingRatingClickListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getComponent(RateThingsComponent.class).inject(this);

        if (savedInstanceState == null) {
            mPagePresenter.onFirstUIAttachment(this);
        } else {
            mPagePresenter.onUIReattached(this);
        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void showPagedThingGrids(PagedThingUIModel pagedThingUIModel) {
        mViewPager.setAdapter(new ThingGridPagerAdapter(getContext(), pagedThingUIModel, new ThingAdapter.OnThingClickListener() {
            @Override
            public void onThingClick(final ThingUIModel thingUIModel) {
                mRatingOverlayDelegate.show(mViewPager.getRateOverlayView(), thingUIModel);
            }
        }));

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mRatingOverlayDelegate.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void markItemRated(ThingUIModel thingUIModel) {
        ThingGridView thingGrid = mViewPager.getThingGridView();
        thingGrid.updateThingRating(thingUIModel);
        mRatingOverlayDelegate.hide();
    }

    @Override
    public void showFetchThingsError() {
        // TODO some retry button
    }


    private RatingOverlayDelegate.OnThingRatingClickListener mOnThingRatingClickListener =
            new RatingOverlayDelegate.OnThingRatingClickListener() {
                @Override
                public void onThingRatingClick(ThingUIModel thingUIModel, ReviewOverlayView.RatingUI rating) {
                    mPagePresenter.onRateThingClick(thingUIModel, rating.toInteger());
                }
            };


    @Override
    public void onDestroyView() {
        mRatingOverlayDelegate.onDestroy();
        mPagePresenter.onUIDetached();
        super.onDestroyView();
    }

    @Override
    public boolean handleBackPressed() {
        return mRatingOverlayDelegate.onBackPressed();
    }
}
