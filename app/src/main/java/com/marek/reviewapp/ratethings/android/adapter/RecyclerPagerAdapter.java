package com.marek.reviewapp.ratethings.android.adapter;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 16.07.16.
 */
public abstract class RecyclerPagerAdapter<T extends RecyclerPagerAdapter.ViewHolder> extends PagerAdapter {

    List<T> cachedViewHolders = new ArrayList<>();

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        T viewHolder = getUnattachedViewHolder();
        viewHolder.attached = true;
        onBindViewHolder(viewHolder, position);
        viewHolder.view.setTag(position);
        container.addView(viewHolder.view);
        return viewHolder;
    }

    protected abstract T onCreateViewHolder();
    protected abstract void onBindViewHolder(T viewHolder, int position);
    protected abstract void onDestroyPage(T viewHolder);

    private T getUnattachedViewHolder() {
        if (!cachedViewHolders.isEmpty()) {
            for (T viewHolder : cachedViewHolders) {
                if (!viewHolder.attached) {
                    return viewHolder;
                }
            }
        }
        T viewHolder = onCreateViewHolder();
        cachedViewHolders.add(viewHolder);
        return viewHolder;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        T viewHolder = (T) object;
        viewHolder.attached = false;
        onDestroyPage(viewHolder);
        container.removeView(viewHolder.view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (object instanceof View && view == (View) object) || (object instanceof ViewHolder && ((ViewHolder) object).view == view);
    }

    public static class ViewHolder {
        public boolean attached;

        public final View view;

        public ViewHolder(View view) {
            this.view = view;
        }
    }

}
