package com.marek.reviewapp.ratethings.android.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.marek.reviewapp.R;

/**
 * Created by marek on 17.07.16.
 */
public class ThingGridViewPager extends ViewPager {

    public ThingGridViewPager(Context context) {
        super(context);
    }

    public ThingGridViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReviewOverlayView getRateOverlayView() {
        return (ReviewOverlayView) getCurrentPageView().findViewById(R.id.page_layout_review_overlay); // TODO avoid calling findViewById; use page changed listener & cache the references
    }

    public ThingGridView getThingGridView() {
        return (ThingGridView) getCurrentPageView().findViewById(R.id.page_layout_thing_grid);
    }

    private View getCurrentPageView() {
        return findViewWithTag(getCurrentItem());
    }

}
