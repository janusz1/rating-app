package com.marek.reviewapp.ratethings.presenter;

import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;
import com.marek.reviewapp.ratethings.domain.model.wrapper.RatedThingEntityList;
import com.marek.reviewapp.ratethings.domain.usecase.FetchThingsUseCase;
import com.marek.reviewapp.ratethings.domain.usecase.RateThingUseCase;
import com.marek.reviewapp.ratethings.presenter.model.PagedThingUIModel;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;
import com.marek.reviewapp.ratethings.presenter.transformer.ThingTransformer;
import com.marek.reviewapp.ratethings.view.PageView;

import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by marek on 16.07.16.
 */
public class PagedThingPresenter {

    private final static int PAGE_SIZE = 9;

    @Inject
    FetchThingsUseCase mFetchThingsUseCase;
    @Inject
    RateThingUseCase mRateThingUseCase;

    private PageView mPageView;

    private ThingTransformer mThingTransformer;

    private PagedThingUIModel mCachedUIModel;

    @Inject
    PagedThingPresenter() {}

    public void onFirstUIAttachment(PageView pageView) {
        mPageView = pageView;
        fetchThings();
    }

    public void onUIReattached(PageView pageView) {
        mPageView = pageView;
        if (mCachedUIModel != null) {
            mPageView.showPagedThingGrids(mCachedUIModel);
        } else {
            fetchThings();
        }
    }

    private void fetchThings() {
        mThingTransformer = new ThingTransformer();

        Observable<RatedThingEntityList> thingListObservable = mFetchThingsUseCase.fetchThings();
        thingListObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RatedThingEntityList>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        mPageView.showFetchThingsError();
                    }

                    @Override
                    public void onNext(RatedThingEntityList thingAndRatingEntities) {
                        ThingEntityList thingEntityList = thingAndRatingEntities.getThingEntityList();
                        Map<Integer, Integer> thingRatings = thingAndRatingEntities.getThingIdRatingMap();
                        mCachedUIModel = mThingTransformer.getPagedThingUIModel(thingEntityList, thingRatings, PAGE_SIZE);
                        mPageView.showPagedThingGrids(mCachedUIModel);
                    }
                });
    }

    public void onRateThingClick(ThingUIModel thingUIModel, Integer rating) {
        thingUIModel.setRating(rating);
        mPageView.markItemRated(thingUIModel);
        mRateThingUseCase.rateThing(thingUIModel.getThingId(), rating);
    }

    public void onUIDetached() {
        mPageView = null;
    }
}
