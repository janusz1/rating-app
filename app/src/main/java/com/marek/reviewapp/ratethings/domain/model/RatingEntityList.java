package com.marek.reviewapp.ratethings.domain.model;

import java.util.List;

/**
 * Created by marek on 16.07.16.
 */
public class RatingEntityList {

    private List<? extends RatingEntity> mRatingEntities;

    public List<? extends RatingEntity> getRatingEntities() {
        return mRatingEntities;
    }

    public void setRatingEntities(List<? extends RatingEntity> ratingEntities) {
        mRatingEntities = ratingEntities;
    }
}
