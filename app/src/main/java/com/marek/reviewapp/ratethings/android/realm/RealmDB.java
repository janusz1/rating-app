package com.marek.reviewapp.ratethings.android.realm;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by marek on 16.07.16.
 */
public class RealmDB {

    public static String DB_NAME = "rating-app-db";
    public static int DB_VERSION = 1;

    private static RealmConfiguration mRealmConfig;

    public void init(Context context) {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(context);
        mRealmConfig = builder.schemaVersion(DB_VERSION)
                .name(DB_NAME)
                .modules(new RatingAppRealmModule())
                .build();
    }

    public static Realm getRatingAppRealm() {
        return Realm.getInstance(mRealmConfig);
    }
}
