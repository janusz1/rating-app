package com.marek.reviewapp.ratethings.domain.repository;

import com.marek.reviewapp.ratethings.domain.model.RatingEntityList;

import rx.Observable;

/**
 * Created by marek on 16.07.16.
 */
public interface RatingRepository extends FetchThingsRepository<RatingEntityList> {

    @Override
    Observable<RatingEntityList> fetchThingData();

    void createOrUpdate(Integer thingId, Integer rating);

}
