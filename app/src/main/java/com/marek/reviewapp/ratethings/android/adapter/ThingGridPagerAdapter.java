package com.marek.reviewapp.ratethings.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.marek.reviewapp.R;
import com.marek.reviewapp.ratethings.android.view.ThingGridView;
import com.marek.reviewapp.ratethings.presenter.model.PagedThingUIModel;

/**
 * Created by marek on 16.07.16.
 */
public class ThingGridPagerAdapter extends RecyclerPagerAdapter<GridVH> {

    private PagedThingUIModel mPagedThingUIModel;
    private ThingAdapter.OnThingClickListener mOnThingClickListener;

    private Context mContext;

    public ThingGridPagerAdapter(Context context, PagedThingUIModel pagedThingUIModel, ThingAdapter.OnThingClickListener onThingClickListener) {
        mPagedThingUIModel = pagedThingUIModel;
        mContext = context;
        mOnThingClickListener = onThingClickListener;
    }

    @Override
    protected GridVH onCreateViewHolder() {
        GridVH gridVH = new GridVH(LayoutInflater.from(mContext).inflate(R.layout.page_layout, null));
        gridVH.thingGrid.init();

        return gridVH;
    }

    @Override
    protected void onBindViewHolder(GridVH viewHolder, int position) {
        ThingAdapter adapter = new ThingAdapter(mContext, mPagedThingUIModel.getThingUIModelPage(position), mOnThingClickListener);
        viewHolder.thingGrid.setAdapter(adapter);
    }

    @Override
    protected void onDestroyPage(GridVH viewHolder) {
    }

    @Override
    public int getCount() {
        return mPagedThingUIModel.getNumPages();
    }

}

class GridVH extends RecyclerPagerAdapter.ViewHolder {

    ThingGridView thingGrid;

    GridVH(View layout) {
        super(layout);
        thingGrid = (ThingGridView) view.findViewById(R.id.page_layout_thing_grid);
    }


}
