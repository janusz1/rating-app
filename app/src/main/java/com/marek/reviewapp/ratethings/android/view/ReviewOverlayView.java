package com.marek.reviewapp.ratethings.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.marek.reviewapp.MainApplication;
import com.marek.reviewapp.R;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by marek on 16.07.16.
 */
public class ReviewOverlayView extends FrameLayout implements Target {

    private ImageView mThumbnail;
    private View mLove;
    private View mLike;
    private View mOK;
    private View mDislike;
    private View mHate;
    private View mRateContainer;
    private View mProgress;

    private OnRatedListener mOnRatedListener;

    public enum RatingUI {  // TODO polymorphic
        LOVE(0), LIKE(1), OK(2), DISLIKE(3), HATE(4);

        Integer mId;

        RatingUI(Integer id) {
            mId = id;
        }

        public Integer toInteger() {
            return mId;
        }

        public static RatingUI fromInteger(Integer id) {
            RatingUI rating = null;
            if (id != null) {
                switch (id.intValue()) {
                    case 0:
                        rating = LOVE;
                        break;
                    case 1:
                        rating = LIKE;
                        break;
                    case 2:
                        rating = OK;
                        break;
                    case 3:
                        rating = DISLIKE;
                        break;
                    case 4:
                        rating = HATE;
                        break;
                    default:
                        throw new IllegalArgumentException("Illegal Rating ID");
                }
            }
            return rating;
        }

        public Drawable getDrawable() {
            Drawable drawable = null;
            @DrawableRes int resId = -1;
            switch (this) {
                case LOVE:
                    resId = R.drawable.love;
                    break;
                case LIKE:
                    resId = R.drawable.like;
                    break;
                case OK:
                    resId = R.drawable.ok;
                    break;
                case DISLIKE:
                    resId = R.drawable.dislike;
                    break;
                case HATE:
                    resId = R.drawable.hate;
                    break;
                default:
                    throw new IllegalStateException("Should not happen");
            }
            if (resId != -1) {
                drawable = MainApplication.getAppContext().getResources().getDrawable(resId);
            }
            return drawable;
        }
    }

    public interface OnRatedListener {
        void onRated(RatingUI rating);
    }

    public void setOnRatedListener(OnRatedListener listener) {
        mOnRatedListener = listener;
    }

    public ReviewOverlayView(Context context) {
        super(context);
        init();
    }

    public ReviewOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ReviewOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ReviewOverlayView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void show(ThingUIModel thing) {
        MainApplication.getPicasso().load(thing.getThumbnail()).into(this);
    }

    public void hide() {
        setLoadedContentVisible(false);
        setVisibility(GONE);
    }

    private void init() {
        View view = inflate(getContext(), R.layout.review_overlay_layout, this);
        mThumbnail = (ImageView) view.findViewById(R.id.rating_overlay_image);
        mRateContainer = view.findViewById(R.id.rating_overlay_rate);
        mProgress = view.findViewById(R.id.rating_overlay_progress);

        mLove = view.findViewById(R.id.rating_overlay_rate_love);
        setRateClickListener(mLove, RatingUI.LOVE);

        mLike = view.findViewById(R.id.rating_overlay_rate_like);
        setRateClickListener(mLike, RatingUI.LIKE);

        mOK = view.findViewById(R.id.rating_overlay_rate_ok);
        setRateClickListener(mOK, RatingUI.OK);

        mDislike = view.findViewById(R.id.rating_overlay_rate_dislike);
        setRateClickListener(mDislike, RatingUI.DISLIKE);

        mHate = view.findViewById(R.id.rating_overlay_rate_hate);
        setRateClickListener(mHate, RatingUI.HATE);
    }

    private void setRateClickListener(View ratingView, final RatingUI rating) {
        ratingView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnRatedListener != null) {
                    mOnRatedListener.onRated(rating);
                }
            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        mOnRatedListener = null;
        super.onDetachedFromWindow();
    }

    // Picasso target iface
    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        mThumbnail.setImageBitmap(bitmap);
        onBitmapLoadingFinished();
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        mThumbnail.setImageDrawable(MainApplication.getAppContext().getResources().getDrawable(android.R.drawable.gallery_thumb));
        onBitmapLoadingFinished();
        Log.e("ReviewOverlayView", "Couldn't load bitmap");
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {
        setLoadedContentVisible(false);
        mProgress.setVisibility(VISIBLE);
        setVisibility(VISIBLE);
    }

    private void onBitmapLoadingFinished() {
        mProgress.setVisibility(GONE);
        setLoadedContentVisible(true);
        setVisibility(VISIBLE);
    }

    private void setLoadedContentVisible(boolean visible) {
        int visibility = visible ? VISIBLE : GONE;
        mThumbnail.setVisibility(visibility);
        mRateContainer.setVisibility(visibility);
    }

}
