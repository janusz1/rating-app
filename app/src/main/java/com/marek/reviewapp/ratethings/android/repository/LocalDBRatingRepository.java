package com.marek.reviewapp.ratethings.android.repository;

import com.marek.reviewapp.ratethings.android.realm.RealmDB;
import com.marek.reviewapp.ratethings.domain.model.RatingEntity;
import com.marek.reviewapp.ratethings.domain.model.RatingEntityList;
import com.marek.reviewapp.ratethings.domain.model.RatingEntityRealm;
import com.marek.reviewapp.ratethings.domain.repository.RatingRepository;

import java.util.List;

import io.realm.Realm;
import rx.Observable;

/**
 * Created by marek on 16.07.16.
 */
public class LocalDBRatingRepository implements RatingRepository {

    @Override
    public Observable<RatingEntityList> fetchThingData() {

        Realm realm = RealmDB.getRatingAppRealm();
//        Observable<RatingEntityList> ratingEntityListObservable = realm.where(RatingEntityRealm.class).findAllAsync()
//                .asObservable().observeOn(AndroidSchedulers.mainThread())
//                .flatMap(new Func1<RealmResults<RatingEntityRealm>, Observable<RatingEntityList>>() {
//                    @Override
//                    public Observable<RatingEntityList> call(RealmResults<RatingEntityRealm> ratingEntityRealms) {
//                        RatingEntityList ratingEntityList = new RatingEntityList();
//                        ratingEntityList.setRatingEntities(ratingEntityRealms);
//                        return Observable.just(ratingEntityList);
//                    }
//                });

        List<RatingEntityRealm> ratings = realm.where(RatingEntityRealm.class).findAll();
        RatingEntityList ratingEntityList = new RatingEntityList();
        List<? extends RatingEntity> ratingsCopy = realm.copyFromRealm(ratings);
        ratingEntityList.setRatingEntities(ratingsCopy);

        return Observable.just(ratingEntityList);
    }

    @Override
    public void createOrUpdate(Integer thingId, Integer rating) {
        RatingEntityRealm ratingEntityRealm = new RatingEntityRealm();

        ratingEntityRealm.setThingId(thingId.intValue());
        ratingEntityRealm.setRating(rating.intValue());
        Realm realm = RealmDB.getRatingAppRealm();
        realm.beginTransaction();

        realm.copyToRealmOrUpdate(ratingEntityRealm);
        realm.commitTransaction();
        realm.close();
    }

}
