package com.marek.reviewapp.ratethings.android.repository;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.marek.reviewapp.MainApplication;
import com.marek.reviewapp.R;
import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;
import com.marek.reviewapp.ratethings.domain.repository.ThingRepository;

import java.io.InputStream;
import java.io.InputStreamReader;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by marek on 16.07.16.
 */
public class LocalJsonThingRepository implements ThingRepository {

    private final static int DATA_RES = R.raw.entities;

    Context mAppContext = MainApplication.getAppContext();

    @Inject
    Gson mGson;

    public LocalJsonThingRepository() {
        MainApplication.getAppComponent().inject(this);
    }

    @Override
    public Observable<ThingEntityList> fetchThingData() {

        return Observable.create(new Observable.OnSubscribe<ThingEntityList>() {
            public void call(Subscriber<? super ThingEntityList> subscriber) {
                try {
                    ThingEntityList thingEntityList = parseJson();
                    subscriber.onNext(thingEntityList);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });

    }

    private ThingEntityList parseJson() throws JsonParseException {
        ThingEntityList thingEntityList = null;
        InputStream dataBytes = mAppContext.getResources().openRawResource(DATA_RES);
        if (dataBytes != null) {
            thingEntityList = mGson.fromJson(
                    new InputStreamReader(dataBytes), ThingEntityList.class);
        }
        return thingEntityList;
    }
}
