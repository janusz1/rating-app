package com.marek.reviewapp.ratethings.domain.model;

import java.util.List;

/**
 * Created by marek on 16.07.16.
 */
public class ThingEntityList {

    private List<ThingEntity> thingEntities;

    public List<ThingEntity> getThingEntities() {
        return thingEntities;
    }

    public void setThingEntities(List<ThingEntity> thingEntities) {
        this.thingEntities = thingEntities;
    }
}
