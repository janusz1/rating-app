package com.marek.reviewapp.ratethings.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marek on 16.07.16.
 */
public class ThingEntity {
    private String label;
    @SerializedName("thing_name")
    private String thingName;
    @SerializedName("thing_id")
    private Integer thingId;
    @SerializedName("thing_type")
    private String thingType;
    private String uuid;
    private String thumbnail;
    @SerializedName("thing_class")
    private String thingClass;
    @SerializedName("rated_class")
    private String ratedClass;
    private Integer reviewable;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getThingName() {
        return thingName;
    }

    public void setThingName(String thingName) {
        this.thingName = thingName;
    }

    public Integer getThingId() {
        return thingId;
    }

    public void setThingId(Integer thingId) {
        this.thingId = thingId;
    }

    public String getThingType() {
        return thingType;
    }

    public void setThingType(String thingType) {
        this.thingType = thingType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThingClass() {
        return thingClass;
    }

    public void setThingClass(String thingClass) {
        this.thingClass = thingClass;
    }

    public String getRatedClass() {
        return ratedClass;
    }

    public void setRatedClass(String ratedClass) {
        this.ratedClass = ratedClass;
    }

    public Integer getReviewable() {
        return reviewable;
    }

    public void setReviewable(Integer reviewable) {
        this.reviewable = reviewable;
    }

}
