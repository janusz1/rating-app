package com.marek.reviewapp.ratethings.domain.usecase;

import com.marek.reviewapp.ratethings.domain.model.RatingEntityList;
import com.marek.reviewapp.ratethings.domain.model.ThingEntityList;
import com.marek.reviewapp.ratethings.domain.model.wrapper.RatedThingEntityList;
import com.marek.reviewapp.ratethings.domain.repository.FetchThingsRepository;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func2;

/**
 * Created by marek on 16.07.16.
 */
public class FetchThingsUseCase {

    @Inject
    FetchThingsRepository<ThingEntityList> mThingsRepository;
    @Inject
    FetchThingsRepository<RatingEntityList> mRatingRepository;

    @Inject
    public FetchThingsUseCase(FetchThingsRepository<ThingEntityList> thingsRepository, FetchThingsRepository<RatingEntityList> ratingRepository) {
        mThingsRepository = thingsRepository;
        mRatingRepository = ratingRepository;
    }

    public Observable<RatedThingEntityList> fetchThings() {
        return mThingsRepository.fetchThingData()
                .zipWith(mRatingRepository.fetchThingData(), new Func2<ThingEntityList, RatingEntityList, RatedThingEntityList>() {
                    @Override
                    public RatedThingEntityList call(ThingEntityList thingEntityList, RatingEntityList ratingEntityList) {
                        return new RatedThingEntityList(thingEntityList, ratingEntityList);
                    }
                });
    }

}
