package com.marek.reviewapp.ratethings.android;

import android.view.View;

import com.marek.reviewapp.ratethings.android.view.ReviewOverlayView;
import com.marek.reviewapp.ratethings.presenter.model.ThingUIModel;

/**
 * Created by marek on 16.07.16.
 */
public class RatingOverlayDelegate {

    private ReviewOverlayView mView;
    private OnThingRatingClickListener mRatingClickListener;

    public interface OnThingRatingClickListener {
        void onThingRatingClick(ThingUIModel thingUIModel, ReviewOverlayView.RatingUI rating);
    }

    public RatingOverlayDelegate(OnThingRatingClickListener ratingClickListener) {
        mRatingClickListener = ratingClickListener;
    }

    public void show(ReviewOverlayView view, final ThingUIModel thingUIModel) {
        mView = view;
        mView.show(thingUIModel);
        mView.setOnRatedListener(new ReviewOverlayView.OnRatedListener() {
            @Override
            public void onRated(ReviewOverlayView.RatingUI rating) {
                mRatingClickListener.onThingRatingClick(thingUIModel, rating);
            }
        });
    }

    public void hide() {
        mView.hide();
    }

    public boolean onBackPressed() {
        boolean handled = false;
        if (mView != null && mView.getVisibility() == View.VISIBLE) {
            hide();
            handled = true;
        }
        return handled;
    }

    public void onPageSelected(int position) {
        if (mView != null) {
            mView.setVisibility(View.GONE);
        }
    }

    public void onDestroy() {
        mRatingClickListener = null;
    }

}
