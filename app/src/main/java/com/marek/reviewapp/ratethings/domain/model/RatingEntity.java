package com.marek.reviewapp.ratethings.domain.model;

/**
 * Created by marek on 16.07.16.
 */
public interface RatingEntity {

    int getThingId();

    void setThingId(int thingId);

    int getRating();

    void setRating(int rating);

}
