package com.marek.reviewapp.ratethings.presenter.model;

/**
 * Created by marek on 16.07.16.
 */
public class ThingUIModel {

    private String label;
    private Integer thingId;
    private String thumbnail;
    private Integer rating;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getThingId() {
        return thingId;
    }

    public void setThingId(Integer thingId) {
        this.thingId = thingId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

}
