package com.marek.reviewapp.android;

import android.support.v4.app.Fragment;

import com.marek.reviewapp.di.HasComponent;

/**
 * Created by marek on 18.07.16.
 */
public abstract class BaseDependencyFragment extends Fragment {
    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
