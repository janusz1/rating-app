package com.marek.reviewapp.android;

/**
 * Created by marek on 18.07.16.
 */
public interface CanHandleBackPress {

    boolean handleBackPressed();

}
