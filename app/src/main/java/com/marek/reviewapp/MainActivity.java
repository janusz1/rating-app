package com.marek.reviewapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.marek.reviewapp.android.CanHandleBackPress;
import com.marek.reviewapp.di.HasComponent;
import com.marek.reviewapp.ratethings.android.PagedThingsFragment;
import com.marek.reviewapp.ratethings.di.DaggerRateThingsComponent;
import com.marek.reviewapp.ratethings.di.RateThingsComponent;
import com.marek.reviewapp.ratethings.di.RateThingsModule;

public class MainActivity extends AppCompatActivity implements HasComponent<RateThingsComponent> {

    private CanHandleBackPress mPagedThingsFragment;
    private RateThingsComponent mRateThingsComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initRateThingsComponent();
        setContentView(R.layout.activity_main);

        initPagedThingsFragment(savedInstanceState != null);
    }

    private void initRateThingsComponent() {
       mRateThingsComponent = DaggerRateThingsComponent.builder().rateThingsModule(new RateThingsModule()).build();
    }

    private void initPagedThingsFragment(boolean isRestored) {
        if (!isRestored) {
            addPagedThingsFragment();
        } else {
            mPagedThingsFragment = (CanHandleBackPress)getSupportFragmentManager().findFragmentByTag(PagedThingsFragment.BACKSTACK_TAG);
        }
    }

    private void addPagedThingsFragment() {
        PagedThingsFragment fragment = PagedThingsFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_fragment_container,
                fragment, PagedThingsFragment.BACKSTACK_TAG).commit();
        mPagedThingsFragment = fragment;
    }

    @Override
    public void onBackPressed() {
        if (mPagedThingsFragment != null && !mPagedThingsFragment.handleBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public RateThingsComponent getComponent() {
        return mRateThingsComponent;
    }
}
